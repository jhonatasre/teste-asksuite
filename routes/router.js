const express = require('express');
const moment = require('moment');
const browserService = require('../services/BrowserService');
const router = express.Router();

router.get('/', (req, res) => {
    res.send('Hello Asksuite World!');
});

router.post('/search', async (req, res) => {
    const browser = await browserService.getBrowser();
    const page = await browser.newPage();

    let checkIn = moment(req.body.checkin);
    let checkOut = moment(req.body.checkout);

    if (checkIn.isValid() && checkOut.isValid()) {
        checkIn = checkIn.format('DDMMYYYY');
        checkOut = checkOut.format('DDMMYYYY');
    } else {
        res.status(400).json({status: 'error', message: 'Check that the dates entered are correct.'});
    }

    const url = `https://book.omnibees.com/hotelresults?CheckIn=${checkIn}&CheckOut=${checkOut}&Code=AMIGODODANIEL&NRooms=1&_askSI=d34b1c89-78d2-45f3-81ac-4af2c3edb220&ad=2&ag=-&c=2983&ch=0&diff=false&group_code=&lang=pt-BR&loyality_card=&utm_source=asksuite&q=5462#show-more-hotel-button`;
    await page.goto(url);

    const list = await page.evaluate(() => {
        const roomRates = Object.values(document.getElementsByClassName('roomrate'));
        return roomRates.map((e) => {
            return {
                name: (e.querySelector('.hotel_name') || {}).innerText || '',
                description: (e.querySelector('.hotel-description') || {}).innerText || '',
                price: (e.querySelector('.price-total') || {}).innerText || 'R$ 0,00',
                image: (e.querySelector('.image-step2') || {}).src  || ''
            }
        });
    });

    browserService.closeBrowser(browser);
    res.status(200).json(list);
});

module.exports = router;
